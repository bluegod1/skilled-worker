class AddUniqueIndexToWorkersSkills < ActiveRecord::Migration
  def change
    remove_index :workers_skills, :name => 'index_workers_skills_on_worker_id_and_skill_id'
    add_index :workers_skills, [:worker_id, :skill_id], :unique => true
  end
end
