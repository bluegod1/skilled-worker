class AddUniqueIndexToSkills < ActiveRecord::Migration
  def change
    remove_index :skills, :name => 'index_skills_on_name'
    add_index :skills, :name, :unique => true
  end
end
