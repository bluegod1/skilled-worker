class CreateCustomersWorkers < ActiveRecord::Migration
  def change
    create_table :customers_workers , id: false do |t|
      t.belongs_to :customer, index: true
      t.belongs_to :worker, index: true

      t.timestamps
    end
    add_index :customers_workers, [:customer_id, :worker_id]
  end
end
