class Customer < User
  has_and_belongs_to_many :workers, join_table: :customers_workers
end
