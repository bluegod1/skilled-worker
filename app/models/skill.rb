class Skill < ActiveRecord::Base
  has_and_belongs_to_many :workers, join_table: :workers_skills
  validates_uniqueness_of :name
  paginates_per 5
end
