class SkillsController < ApplicationController
  before_action :auth_user!, :except => [:index]

  def index
   @skills = Skill.all.page skills_params[:page]
  end

  def search
    @skills = paginate_skills
  end

  private

  def paginate_skills
    #TODO: add per_page as a config thing
    skills.page skills_params[:page]
  end

  def skills
    Search::SkillFinder.find(keywords: keywords)
  end

  def skills_params
    @_skills_params ||= params.permit(:page, :keywords)
  end

  def keywords
    skills_params[:keywords].split(',') if skills_params[:keywords]
  end
end
