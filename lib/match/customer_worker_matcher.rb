class Match::CustomerWorkerMatcher

  def initialize(customer: , worker: )
    @customer = customer
    @worker = worker
  end

  def self.match!(*args)
    new(*args).match!
  end

  def match!
    @customer.workers << @worker
    @customer.save!
  end
end
