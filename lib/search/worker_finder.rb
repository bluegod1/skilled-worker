class Search::WorkerFinder
  include Db::TableUtils

  def initialize(skills:)
    @skills = skills.pluck(:id)
  end

  def self.find(*args)
    new(*args).find
  end

  def find
    return Worker.none if @skills.empty?
    run_query
  end

  private

  def run_query
    Worker.joins('inner join workers_skills ws on users.id = ws.worker_id')
      .where(workers_skills[:skill_id]
               .in_any(@skills))
      .group(workers[:id])
      .having(workers_skills[:skill_id].count.eq(@skills.size))
  end
end
