class Search::WorkerWithSkillFinder

  def initialize(worker_finder: Search::WorkerFinder, skill_finder: Search::SkillFinder, keywords:)
    @keywords = keywords
    @worker_finder = worker_finder
    @skill_finder = skill_finder
  end

  def self.find(*args)
    new(*args).find
  end

  def find
    @worker_finder.find(skills: skills)
  end

  private

  def skills
    @skill_finder.find(keywords: @keywords)
  end
end