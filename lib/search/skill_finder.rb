class Search::SkillFinder
  include Db::TableUtils

  def initialize(keywords:)
    @keywords = keywords
  end

  def self.find(*args)
    new(*args).find
  end

  def find
    @keywords.blank? ? Skill.all : Skill.where(match_clause)
  end

  private

  def match_clause
    skills[:name].matches_any(@keywords.map { |k| "%#{k}%" })
  end
end