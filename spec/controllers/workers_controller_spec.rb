require 'rails_helper'

RSpec.describe WorkersController, type: :controller do

  context 'customer' do
    let!(:skill1) { create(:skill, name: 'ironing') }
    let!(:skill2) { create(:skill, name: 'drying') }
    let!(:customer) { create(:customer) }
    let!(:worker1) { create(:worker, skills: [skill1]) }
    let!(:worker2) { create(:worker, skills: [skill2]) }

    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:customer]
      sign_in :customer, customer
    end

    describe 'search for workers with a skill' do
      it 'assigns @workers with a skill list' do
        xhr :get, :index, { keywords: 'iron' }
        expect(assigns(:workers)).to eq([worker1])
      end

      it 'asserts @workers is empty whith no results' do
        xhr :get, :index, { keywords: 'hello' }
        expect(assigns(:workers)).to eq([])
      end
    end

    describe 'links a customer with a worker' do
      it 'assigns @skills list' do
        expect {
          xhr :get, :customer, id: worker1.id }
          .to change { worker1.reload.customers.size }.by(1)
      end
    end
  end

  context 'worker' do
    let!(:worker) { create(:worker) }
    let!(:customer) { create(:customer) }
    let!(:skill1) { create(:skill, name: 'ironing') }

    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:worker]
      sign_in :worker, worker
    end

    it 'adds a skill to worker' do
      expect {
        xhr :get, :skill, name: skill1.name }
        .to change { worker.reload.skills.size }.by(1)
    end
  end
end
