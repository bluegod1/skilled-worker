require 'rails_helper'

RSpec.describe SkillsController, type: :controller do


  context 'worker' do
    let(:skill) { create(:skill) }
    let(:worker) { create(:worker) }

    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:worker]
      sign_in :worker, worker
    end

    describe 'GET index' do
      it 'assigns @skills list' do
        get :index
        expect(assigns(:skills)).to eq([skill])
      end
    end
  end

  context 'customer' do
    let!(:skill1) { create(:skill, name: 'ironing') }
    let!(:skill2) { create(:skill, name: 'drying') }
    let!(:customer) { create(:customer) }

    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:customer]
      sign_in :customer, customer
    end

    describe 'GET index' do
      it 'assigns @skills list' do
        get :index
        expect(assigns(:skills)).to eq([skill1, skill2])
      end
    end

    describe 'search for workers with a skill' do
      it 'assigns @skills with a skill list' do
        xhr :get, :search, {keywords: 'iron'}
        expect(assigns(:skills)).to eq([skill1])
      end

      it 'asserts @skills is empty' do
        xhr :get, :search, {keywords: 'hello'}
        expect(assigns(:skills)).to eq([])
      end
    end
  end
end
