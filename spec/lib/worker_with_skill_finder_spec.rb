require 'rails_helper'

describe Search::WorkerWithSkillFinder, type: :feature do
  let!(:skill1) { create(:skill, name: 'ironing') }
  let!(:skill2) { create(:skill, name: 'drying') }
  let!(:worker1) { create(:worker, skills: [skill1]) }
  let!(:worker2) { create(:worker, skills: [skill2]) }
  let!(:worker3) { create(:worker, skills: [skill1, skill2]) }

  it 'find a worker that has skill 1' do
    expect(described_class.find(keywords: ['ironing'])).to match_array([worker1, worker3])
  end

  it 'finds a worker that has ~ skill 1' do
    expect(described_class.find(keywords: ['iron'])).to match_array([worker1, worker3])
  end

  it 'finds a worker that has both skill1 and skill2' do
    expect(described_class.find(keywords: ['iron', 'dry'])).to match_array([worker3])
  end

  it 'returns an empty list when there is no match' do
    expect(described_class.find(keywords: ['blah'])).to match_array([])
  end
end
